;==========================================================
;
;
;   ██████╗  ██████╗ ██╗  ██╗   ██╗██████╗  █████╗ ██████╗
;   ██╔══██╗██╔═══██╗██║  ╚██╗ ██╔╝██╔══██╗██╔══██╗██╔══██╗
;   ██████╔╝██║   ██║██║   ╚████╔╝ ██████╔╝███████║██████╔╝
;   ██╔═══╝ ██║   ██║██║    ╚██╔╝  ██╔══██╗██╔══██║██╔══██╗
;   ██║     ╚██████╔╝███████╗██║   ██████╔╝██║  ██║██║  ██║
;   ╚═╝      ╚═════╝ ╚══════╝╚═╝   ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝
;
;
;   To learn more about how to configure Polybar
;   go to https://github.com/polybar/polybar
;
;   The README contains a lot of information
;
;==========================================================

[colors]
;background = ${xrdb:color0:#222}
;background = #383c4a ;grey
background = #00000
background-alt = #444
;foreground = ${xrdb:color7:#222}
;foreground = #b0b5bd ;blue
foreground = #1afe49
foreground-alt = #7a04eb
;foreground-alt = #555 ;grey
primary = #ffb52a
secondary = #e60053
alert = #bd2c40

[bar/mybar]
;monitor = ${env:MONITOR:HDMI-1}
width = 100%
height = 30
;offset-x = 1%
;offset-y = 1%
;radius = 6.0
fixed-center = true

background = ${colors.background}
foreground = ${colors.foreground}

line-size = 3
line-color = #000000

border-size = 0
border-color = #1afe49
;border-color = #5294e2 ;blue
;#00000000

padding-left = 2
padding-right = 2

module-margin-left = 1
module-margin-right = 2

;font-0 = fixed:pixelsize=10;1
;font-1 = unifont:fontformat=truetype:size=8:antialias=false;0
;font-2 = siji:pixelsize=10;1
;font-0 = SFNS Display:Regular:size=12;0
font-0 = roboto-regular:size=12;0 
;notosans-regular:size=11;0
font-1 = Font Awesome 5 Free-regular:size=11;0
font-2 = Font Awesome 5 Brands-regular:size=11;0
font-3 = Font Awesome 5 Free Solid-regular:size=11;0
font-4 = fixed:pixelsize=10;1

modules-left = workspaces xwindow
modules-center = mpd
modules-right = filesystem pulseaudio memory cpu wlan eth temperature date powermenu


tray-position = right
tray-padding = 2
tray-background = #7a04eb
;tray-background = #0063ff

;wm-restack = bspwm
;wm-restack = i3

;override-redirect = true


;scroll-up = i3wm-wsnext
;scroll-down = i3wm-wsprev

cursor-click = pointer
cursor-scroll = ns-resize

[module/xwindow]
type = internal/xwindow
label = | %{F#7a04eb}%title:0:30:...%


[module/filesystem]
type = internal/fs
interval = 25

mount-0 = /

label-mounted =   %{F#7a04eb}%percentage_used%% %{F-}
;label-mounted-foreground = ${colors.foreground-alt}
;label-mounted =  %{F#0a81f5}%mountpoint%%{F-}: %percentage_used%%
label-unmounted = %mountpoint% not mounted
label-unmounted-foreground = ${colors.foreground-alt}

;Workspaces
[module/workspaces]
type = internal/i3

pin-workspaces = false
strip-wsnumbers = true
index-sort = false

radius = 6.0

ws-icon-0 = 1;
ws-icon-1 = 2;
ws-icon-2 = 3;
ws-icon-3 = 4;
ws-icon-4 = 5;
ws-icon-5 = 6;
ws-icon-6 = 7;
ws-icon-7 = 8;
ws-icon-8 = 9;
ws-icon-9 = 10;
;ws-icon-default = 

format = <label-state> <label-mode>

label-dimmed-underline = ${colors.background}

;On editing mode (resize for ex)
label-mode = %mode%
label-mode-background = ${colors.dark-purple}
label-mode-underline = ${colors.fav-purple}
label-mode-padding = 2
;label-mode-font = 2

label-focused = %icon% %name% 
label-focused-background = #1afe49
;#5294e2
;#b0b5bd
label-focused-foreground = #383c4a
label-focused-padding = 2
;label-focused-font = 2

label-unfocused = %icon% %name% 
label-unfocused-foreground = ${colors.gray}
label-unfocused-padding = 3
;label-unfocused-font = 3

label-visible = %icon% %name% 
label-visible-background = ${colors.dark-purple}
label-visible-underline = ${colors.light-purple}
label-visible-padding = 3
;label-visible-font = 3

label-urgent = %icon% %name% 
label-urgent-background = ${colors.dark-red}
label-urgent-underline = ${colors.light-red}
label-urgent-padding = 3
;label-urgent-font = 3


[module/mpd]
type = internal/mpd
format-online = <label-song>  <icon-prev> <icon-stop> <toggle> <icon-next>

icon-prev = 
icon-stop = 
icon-play = 
icon-pause = 
icon-next = 

label-song-maxlen = 25
label-song-ellipsis = true

[module/cpu]
type = internal/cpu
interval = 2
format-prefix = " "
;format-prefix-foreground = #b0b5bd
#format-underline = #f90000
label = %{F#7a04eb}%percentage:2%% %{F-}

[module/memory]
type = internal/memory
interval = 2
format-prefix = " "
;format-prefix-foreground = #b0b5bd
#format-underline = #4bffdc
label = %{F#7a04eb}%percentage_used%% %{F-}

[module/wlan]
type = internal/network
interface = net1
interval = 3.0

format-connected = <ramp-signal> <label-connected>
format-connected-underline = #9f78e1
label-connected = %essid%

format-disconnected =
;format-disconnected = <label-disconnected>
;format-disconnected-underline = ${self.format-connected-underline}
;label-disconnected = %ifname% disconnected
;label-disconnected-foreground = ${colors.foreground-alt}

ramp-signal-0 = 
ramp-signal-1 = 
ramp-signal-2 = 
ramp-signal-3 = 
ramp-signal-4 = 
ramp-signal-foreground = ${colors.foreground-alt}

[module/eth]
type = internal/network
interface = enp0s3
interval = 3.0

format-connected-underline = #55aa55
format-connected-prefix = " "
format-connected-prefix-foreground = ${colors.foreground-alt}
label-connected = %local_ip%

format-disconnected =
;format-disconnected = <label-disconnected>
;format-disconnected-underline = ${self.format-connected-underline}
;label-disconnected = %ifname% disconnected
;label-disconnected-foreground = ${colors.foreground-alt}

[module/date]
type = internal/date
interval = 5

date =
date-alt = " %Y-%m-%d"

time = %H:%{F#7a04eb} %M
time-alt = %H:%{F#7a04eb} %M:%S %{F-}

format-prefix = 
format-prefix-foreground = ${colors.foreground-alt}
#format-underline = #0a6cf5

label = %date% %time%

[module/pulseaudio]
type = internal/pulseaudio

format-volume = <label-volume> <bar-volume>
label-volume =  
;%percentage%%
label-volume-foreground = ${root.foreground}

label-muted = 🔇 muted
label-muted-foreground = #666

bar-volume-width = 10
bar-volume-foreground-0 = #7a04eb
;#5294e2
;#55aa55
bar-volume-foreground-1 = #55aa55
bar-volume-foreground-2 = #55aa55
bar-volume-foreground-3 = #55aa55
bar-volume-foreground-4 = #55aa55
bar-volume-foreground-5 = #f5a70a
bar-volume-foreground-6 = #7a04eb
;#5294e2
;#ff5555
bar-volume-gradient = false
bar-volume-indicator = 
bar-volume-indicator-font = 2
bar-volume-fill = ─
bar-volume-fill-font = 2
bar-volume-empty = ─
bar-volume-empty-font = 2
bar-volume-empty-foreground = ${colors.foreground-alt}


[module/temperature]
type = internal/temperature
thermal-zone = 0
warn-temperature = 60

format = <ramp> <label>
;format-underline = #f50a4d
format-warn = <ramp> <label-warn>
format-warn-underline = ${self.format-underline}

label = %{F#7a04eb} %temperature-c% %{F-}
label-warn = %temperature-c%
label-warn-foreground = ${colors.secondary}

ramp-0 = 
ramp-1 = 
ramp-2 = 
;ramp-foreground = #b0b5bd

[module/powermenu]
type = custom/menu

expand-right = true

format-spacing = 1

label-open = 
label-open-foreground = ${colors.secondary}
label-close =  cancel
label-close-foreground = ${colors.secondary}
label-separator = |
label-separator-foreground = ${colors.foreground-alt}

menu-0-0 = reboot
menu-0-0-exec = menu-open-1
menu-0-1 = power off
menu-0-1-exec = menu-open-2

menu-1-0 = cancel
menu-1-0-exec = menu-open-0
menu-1-1 = reboot
menu-1-1-exec = sudo reboot

menu-2-0 = power off
menu-2-0-exec = sudo poweroff
menu-2-1 = cancel
menu-2-1-exec = menu-open-0

[settings]
screenchange-reload = true
;compositing-background = xor
;compositing-background = screen
;compositing-foreground = source
;compositing-border = over
;pseudo-transparency = false

[global/wm]
margin-top = 5
margin-bottom = 5

; vim:ft=dosini
