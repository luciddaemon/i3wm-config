# i3wm


Contains all files for my i3 profile

**File locations:**
- Rename i3config to config and place it in ~/.config/i3/
- Extract and rename i3Scripts to scripts and place it in ~/.config/i3/
- Rename dunstrcConfig to dunstrc and place it in ~/.config/dunst/
- Place settings.ini in ~/.config/gtk-3.0/
- Rename PolybarConfig to config and place it in ~/.config/polybar/
- Place launch.sh in ~/config/polybar/
- Place the contents of rofi.zip in ~./config/rofi
- Place picom.conf in ~/.config/

There are two themes configured in the files:
   - One being a Black/purple/green theme
   - Other being a minimilist grey and blue theme

All config files have both colors, uncomment or comment the correct parts to enable/disable the colors.

_Black/Purple/Green is the current active theme_



**Below are the required GTK themes and icons to replicate the current setup**
**All are in the required-theme.zip file**

THEMES
===========
- Yaru-Blue-Dark
- Flat-Remix-GTK-Green-Darkest-Solid-NoBorder

Put in ~./themes folder

ICONS
===========
- Yaru-Blue
- Flatery-Teal-Dark

Put in ~./icons folder

CURSOUR
===========
- Vimix Cursours

Put in ~./themes folder

FONT
===========
- Roboto Regular 12
- System San Francisco 10
- Font Awesome

Put in ~./fonts folder
